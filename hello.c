#include <stdio.h>
#include <string.h>
#include <math.h>

#include <unistd.h> // for sleep function

#include <libserialport.h> // cross platform serial port lib

const char* desired_port = "COM1";

struct sp_port *port;

void list_serialports() {
	int i;
	struct sp_port **ports;

	enum sp_return error = sp_list_ports(&ports);
	if (error == SP_OK) {
		for (i = 0; ports[i]; i++) {
			printf("Found port: '%s'\n", sp_get_port_name(ports[i]));
		}
		sp_free_port_list(ports);
	} else {
		printf("No serial devices detected\n");
	}
	printf("\n");
}

void parse_serial(char *byte_buff, int byte_num) {
	for(int i = 0; i < byte_num;i++){
		printf("%c", byte_buff[i]);
	}
	printf("\n");
}

int callHello() {
	list_serialports();

	printf("Opening port '%s' \n", desired_port);
	enum sp_return error = sp_get_port_by_name(desired_port,&port);
	if (error == SP_OK) {
		error = sp_open(port,SP_MODE_READ_WRITE);
		if (error == SP_OK) {
			sp_set_baudrate(port,115200);
			sp_set_parity(port,SP_PARITY_NONE);
			sp_set_stopbits(port,1);
			sp_set_bits(port,8);
			sp_blocking_write(port, "hello", 6, 0);
 			sp_drain(port);
			char buf_read[6];
			sp_blocking_read(port,buf_read,5,0);
			parse_serial(buf_read,5);
			fflush(stdout);
			sp_close(port);
		} else {
			printf("Error opening serial device\n");
		}
	} else {
		printf("Error finding serial device\n");
	}


	return 0;
}

