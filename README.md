# Ambiente

  - Instalar MinGW/MSYS2 https://github.com/orlp/dev-on-windows/wiki/Installing-GCC--&-MSYS2 até item 6
```sh
pacman -Syuu
pacman -Syuu
pacman -S --needed base-devel mingw-w64-i686-toolchain mingw-w64-x86_64-toolchain \
                   git subversion mercurial \
                   mingw-w64-i686-cmake mingw-w64-x86_64-cmake \
                   swig
```
  - Instalar chocolatey https://chocolatey.org/install e em seguida os seguintes pacotes
```sh
choco install jdk8
choco install visualstudio2017community
```

- Compilando o código. Abrir o shell MSYS2 **MinGW64**

-- libserialport
```sh
git clone --recursive https://fabricio_ceolin@bitbucket.org/fabricio_ceolin/hello_swig.git
cd hello_swig/libserialport
./autogen.sh
./configure
make
make install
```
-- Codigo Java
```sh
swig -java hello.i
gcc -c hello.c hello_wrap.c -I /c/Program\ Files/Java/jdk1.8.0_144/include/ -I /c/Program\ Files/Java/jdk1.8.0_144/include/win32/
gcc -shared hello.o  hello_wrap.o -Wl,--add-stdcall-alias  -o hello.dll -lserialport
/c/Program\ Files/Java/jdk1.8.0_144/bin/javac.exe main.java
/c/Program\ Files/Java/jdk1.8.0_144/bin/java.exe main
```

-- Código Csharp
```sh
swig -csharp hello.i
gcc -fpic -c hello.c hello_wrap.c
gcc -shared hello.o  hello_wrap.o -Wl,--add-stdcall-alias  -o hello.dll -lserialport
/c/Windows/Microsoft.NET/Framework/v4.0.30319/csc.exe -platform:x64 -t:exe -out:main.exe *.cs
```
