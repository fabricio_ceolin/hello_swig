%module hello

// Add necessary symbols to generated header
%{
#include "hello.h"
%}

// Process symbols in header
%include "hello.h"
